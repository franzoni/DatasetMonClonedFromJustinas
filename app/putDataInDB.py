import time
import sys
from Acquisition_era_class import *
from Dataset_class import *
from RunContent_class import *
from LSContent_class import *
from databaseFunc import *

timerStart = time.time()
AE_Input = sys.argv[1]
acquisition_era = Acquisition_era(AE_Input)
listDatasets = acquisition_era.listDatasets(primaryDataset = "*", dataTier = "*")
session = Session()
k = 0
s = 0
totalLSCounter = 0
totalRunCounter = 0
print ""
for dataset in listDatasets:
		k+=1
		#Check if dataset is already on DB
		check = []
		for element in session.query(dataset_table.datasetName).all():
			dat = str(element)[3:-3]
			check.append(dat)
		if 0<=k and not dataset.datasetName in check:
			#Store:
			s+=1
			dataset_DB = dataset_table(datasetName = dataset.datasetName, primaryDatasetName = dataset.primaryDatasetName,
				processed_ds_name = dataset.processed_ds_name, dataTier = dataset.dataTier,
				acquisition_era_name = dataset.acquisition_era_name, isParent = 0)
			print ("---- Storing Datasets. DS count: %s   DS name: %s") %(k, dataset_DB.datasetName)
			listRuns = acquisition_era.listRuns(dataset.datasetName)
			j = 0
			for run in listRuns: 
				numberEvents = acquisition_era.listEvent(dataset.datasetName, run.runNumber)
				runContent = runContent_table(runNumber = run.runNumber, numberEvents = numberEvents)
				print ("---- Storing Runs. Run count: %s   Run number: %s") %(j, runContent.runNumber)
				dataset_DB.runContents.append(runContent)
				listLS = acquisition_era.listLS(dataset.datasetName, run.runNumber)
				j += 1
				totalRunCounter += 1
				for lumisection in listLS:
					# Store :
					lsContent = lsContent_table(lumiSectionNumber = lumisection, numberEvents = 0)
					runContent.lsContents.append(lsContent)
					totalLSCounter += 1
					session.add(lsContent)
				session.add(runContent)
			session.add(dataset_DB)
			session.commit()
print ""

for dataset in session.query(dataset_table).order_by(dataset_table.id):
	print ("---- Assigned Parent or Child status: %s") %dataset.datasetName
	children = acquisition_era.constructListChildren(dataset.datasetName)
	if not children:
		dataset.isParent = 0
	else:
		dataset.isParent = 1
	for child in children:
		child_DB = session.query(dataset_table).filter(dataset_table.datasetName == child.datasetName).all()
		if not child_DB:
			print ("* ! * ! * ! * Child --> Parent relation NOT created: %s") %child.datasetName
		else:
			dataset.children.append(child_DB[0])
			print ("---- Child --> Parent relation created: %s") %child_DB[0].datasetName
	session.commit()
timerEnd = time.time()
print ("---- %s Datasets exist in Run%s aqcuisition era.") %(k, AE_Input)
print ("---- %s Datasets stored.") %s
print ("---- %s Runs stored.") %totalRunCounter
print ("---- %s Lumisections stored.") %totalLSCounter
print ("---- Completed in: %s seconds.") %(timerEnd - timerStart)







