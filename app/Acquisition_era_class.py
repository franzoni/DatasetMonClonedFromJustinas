import sys, time, sqlite3
from dbs.apis.dbsClient import DbsApi
#url="https://cmsweb.cern.ch/dbs/prod/global/DBSReader"
from time import gmtime
from Dataset_class import *
from RunContent_class import *

class Acquisition_era:
	""" Defined by a period of time (ex : Run2015B)
	    this class uses API to easily find, and print
	    on screen lists related to itself.

	    IMPORTANT : For certain functions, if you want the fonction to print, you have to set doPrint = true when calling the function

	    shortcuts : PD = Primary Dataset
			DS = Dataset
			LS = Lumi Section



	"dataset": 
	"status": "VALID",
	"modified_by": "tier0@vocms15.cern.ch",
	"physics_group_name": "NoGroup",
	"acquisition_era_name": "Run2015B",
	"prep_id": None,
	"creation_time": "2015-06-07 13:20:22",
	"created_by": "tier0@vocms15.cern.ch",
	"processed_ds_name": "Run2015B-v1",
	"modification_time": "2015-06-07 13:20:22",
	"datatype": "data",
	"dataset_id": 11509166,
	"xtcrosssection": None,
	"processing_version": 1,
	"primary_dataset": {
	"name": L1TechBPTXPlusOnly
	},
	"data_tier_name": "RAW",
	"name": /L1TechBPTXPlusOnly/Run2015B-v1/RAW
	"""
	#url="https://cmsweb.cern.ch/dbs/prod/global/DBSReader"
	#api=DbsApi(url=url)
		
	
	def __init__(self, acquisition_era_name):
		self.url = "https://cmsweb.cern.ch/dbs/prod/global/DBSReader"
		self.api = DbsApi(url=self.url)
		self.acquisition_era_name = acquisition_era_name
	
	def getAcquisition_era_name(self):
		return self.acquisition_era_name

	def checkIfParent(self,dataset_name):
		""" check if this dataset has a parent, and if yes, attribute it 
		"""
		parents = api.listDatasetParents(dataset_name)
		return parents

	def constructListChildren(self,datasetName):
		""" construct the list of children
		"""
		listChildren = self.listChildren(datasetName)
		return listChildren

	def constructListRuns(self,dataset):
		""" construct the list of runs
		"""
		listRuns = self.listRuns(dataset.datasetName)
		#for run in listRuns:
			#ACCESS DB

	def constructListLS(self,dataset, run):
		""" construct the list of LS
		"""
		numberLS = self.listLS(dataset.datasetName,run.runNumber) + 1
		i = 1
		while i<numberLS:
			run.lumisection
			

	def listPD(self, doPrint = False): # /!\ /!\ /!\ This function has to be modified
		""" list all the Primary Datasets
		"""
		__result = self.api.listPrimaryDatasets()
		if doPrint:
			for line in __result:
				print (line['primary_ds_name'])
		
		return __result
		

	def listDatasets(self, primaryDataset, dataTier, doPrint = False): #Modified : 23/07 15:00 -- Tested : works
		""" list all the datasets from a choosen type (ex : RAW,AOD...) for a given primary dataset
		"""
		datasetName="/%s/*%s*/%s" %(primaryDataset, self.acquisition_era_name, dataTier)
		__result = []
		API = self.api.listDatasets(dataset=datasetName)
		sortedAPI = sorted(API, key=lambda k: k['dataset'])
		for line in API:
			dataset = Dataset(line['dataset'])
			__result.append(dataset)
			if doPrint:
				print(line['dataset'])
		return __result

	def listChildren(self, datasetName, doPrint = False): #Modified : 23/07 15:12
		""" list all the child datasets for a given dataset
		"""		
		__result = []
		API = self.api.listDatasetChildren(dataset=datasetName)
		#print "API ret : %s" % (API)
		for line in API:
			#print "\t DEBUG %s" % (line)
			dataset = Dataset(line['child_dataset'])
			__result.append(dataset)
			if doPrint:
				print(line)
		return __result

	def listChild_TypeAll(self, dataTier, doPrint = False): # /!\ /!\ /!\ This function has to be modified. Delet ?
		""" list all the child datasets for a given dataset type (ex : RAW,AOD...) 
		"""
		__result = []
		for datasetName in self.listDatasets(dataTier,"*"):
			__temp = self.api.listDatasetChildren(dataset=datasetName['dataset'])
			for line in __temp:
				__result.append(line)
				if doPrint:
					print(line)
			
		return __result
		
	def listChildren_All(self, doPrint = False): # /!\ /!\ /!\ This function has to be modified. Delet ?
		""" list all the child datasets for all datasets 
		"""
		__result = []
		for datasetName in self.listDatasets("*","*"):
			__result = self.api.listDatasetChildren(dataset=datasetName['dataset'])
			for line in __result:
				__result.append(line)
				if doPrint:
					print(line)
		return __result

	def listRuns(self, datasetName, doPrint = False): #Modified : 23/07 15:30 -- Tested : works
		""" list all the Runs for a given dataset
		"""
		__result = []
		API = self.api.listRuns(dataset=datasetName)
	
		for line in API:
			for num in line['run_num']:
				runContent = RunContent(num)
				__result.append(runContent)
				if doPrint:
	  				print(num)
		return __result

	def listRuns_TypeAll(self, dataTier, doPrint = False): # /!\ /!\ /!\ This function has to be modified. Delet ?
		""" list all the Runs for a given dataset type (ex : RAW,AOD...)
		"""
		__result = []
		for datasetName in self.listdataTier_TypeAll(dataTier):
			__temp = self.api.listRuns(dataset=datasetName['dataset'])
			for line in __temp:
				__result.append(line)
				if doPrint:
					for num in line['run_num']:
						print(num)
		return __result

	def listRuns_All(self, doPrint = False): # /!\ /!\ /!\ This function has to be modified. Delet ?
		""" list all the Runs for all datasets
		"""	
		__result = []
		for datasetName in self.listdataTier_All():
			__temp = self.api.listRuns(dataset=datasetName['dataset'])
	       		for line in __temp:
				__result.append(line)
				if doPrint:
					for num in line['run_num']:
						print(num)
		return __result

	def listEvent(self, datasetName,Run, doPrint = False): # /!\ /!\ /!\ This function has to be modified.
		""" give the number of events for a given Run in a given dataset
		"""
		__result = self.api.listFileSummaries(dataset=datasetName, run_num=Run)
		if doPrint:
			print (__result[0]['num_event'])
		return __result[0]['num_event']

	def listEvent_All(self, datasetName, doPrint = False): # /!\ /!\ /!\ This function has to be modified. Delet ?
		""" give the numbers of events for all Runs in a given dataset
		"""
		for Run in self.listRuns(datasetName):
			for num in Run['run_num']:
				self.listEvent(datasetName, num, doPrint)

	def listLS(self, datasetName,Run, doPrint = False): # /!\ /!\ /!\ This function has to be modified.
		""" give the number of Lumi Sections for a given Run 
		"""
		file_detail = self.api.listFiles(dataset=datasetName, run_num=Run, detail = True)
		__result = self.api.listFileLumis(logical_file_name = file_detail[0]['logical_file_name'])
		if doPrint:
			print (__result[0]['lumi_section_num'])	
		return __result[0]['lumi_section_num']

	def listLS_All(self, datasetName, doPrint = False): # /!\ /!\ /!\ This function has to be modified. Delet ?
		""" give the numbers of Lumi Sections for all Runs
		"""
		for Run in self.listRuns(datasetName):
			for num in Run['run_num']:
				self.listLS(datasetName, num, doPrint)
	
