----Launching the dataset monitoring service----

1. Go to "openstack.cern.ch" and create a large or medium sized VM using "SLC6 CERN Server" latest image. When creating your VM, go to "Access and Security" tab, and add the public key of your local machine for easy access. Access your machine using secure shell: "ssh -i path_to_your_local_public_key username@ip_or_name_of_remote_host" or use secure shell to login with password as in step 2
2. Log in: "ssh username@ip_or_name_of_remote_host"
3. Follow tutorial on deploying in this link: cms-conddb-prod.cern.ch/docs/deploying.html
4. Go to "/data/services" and clone the "DatasetMon" folder from git repo
5. Edit "/data/services/keeper/config.py" and add the following: "'DatasetMon':{ 'filename':'run.py', 'listeningPort':9999, },"
6. Run the following commands:
	cd /data/service/DatasetMon
	source /data/cmssw/setupEnv.sh
	virtualenv --python=python2.7
	venv source ./venv/bin/activate
	pip install -r requirements.txt
7. Add the following to "/data/services/keeper/keeper.py" in the "getPIDs(service)" method: "elif service == 'DatasetMon': pids = _getPIDs(' run.py --name %s ' % service )"
8. Run the following command: "sudo iptables -I INPUT 5 -p tcp -m state --state NEW -m tcp --dport 9999 -j ACCEPT"
9. Start the service by running: "./keeper.py start DatasetMon"
10. Open browser, type the following to the address bar to see if it works: "name_of_VM.cern.ch:9999" or "ip_of_VM:9999"


----Creating database if it doesn't exist----

1. Log in to your VM
2. Do NOT set up environment
3. Navigate to /data/services/DatasetMon/app/
4. Run: python databaseFunc.py


----Filling the database----

1. Log in to your VM
2. Set up environment:
	eval `scramv1 runtime -sh`
	source  /afs/cern.ch/cms/ccs/wm/scripts/Crab/crab.sh
	voms-proxy-init
3. Navigate to /data/services/DatasetMon/app/
4. Run: python putDataInDB.py "acquisition era"
	For example: python putDataInDB.py 2015C